<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function index()
	{
		//Defino variables
		$producto_id = 10251;
		$descripcion = "Coca cola";
		$precio      = 50;

		//Envio las variables a la vista
		$data["producto_id"] = $producto_id;
		$data["descripcion"] = $descripcion;
		$data["precio"] = $precio;

		$this->load->view('welcome_message',$data);
	}

	public function categorias(){
		//Cargamos el modelo
		$this->load->model('categoria_model');

		//Traemos los registros de la tabla categoria
		$categorias = $this->categoria_model->read();

		//Enviamos los registros a la vista
		$data["registros"] = $categorias;
		$data["finicio"] = "11/03/1978";
		$data["fin"]    = "11/05/1978";
		$this->load->view('categoria_index',$data);
	}

	public function nueva_categoria(){
		$this->load->view('categoria_nueva');
	}

	public function insertar_categoria(){
		$this->load->model('categoria_model');
		$this->load->helper('url');

		//Capturar categoria 
		$descripcion = $this->input->post('descripcion');

		//Insertar en la base de datos
		//echo $descripcion;
		$datos = new stdClass();
		$datos->descripcion = $descripcion;
		$this->db->insert('categoria', $datos);

		//Redireccionar al controlador index
		redirect('/welcome/categorias/');
	}
}
